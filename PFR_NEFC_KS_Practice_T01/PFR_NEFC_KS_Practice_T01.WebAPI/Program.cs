using Microsoft.EntityFrameworkCore;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Mapper;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Service;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Context;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.IRepository;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.Repository;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.UnitOfWork;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AppDbContext>(option => option.UseMySql(builder.Configuration.GetConnectionString("ProductConnection"), ServerVersion.Parse("8.4.0-mysql")));

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IOrderService, OrderService>();
builder.Services.AddScoped<IOrderItemService, OrderItemService>();

builder.Services.AddAutoMapper(typeof(AutoMapperProfile));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
