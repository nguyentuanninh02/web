﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Dto;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;

namespace PFR_NEFC_KS_Practice_T01.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItemController : ControllerBase
    {
        private readonly IOrderItemService _orderItemService;
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public OrderItemController(IOrderItemService orderItemService, IOrderService orderService, IProductService productService, IMapper mapper)
        {
            _orderItemService = orderItemService;
            _orderService = orderService;
            _productService = productService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrderItem()
        {
            try
            {
                var orderItem = await _orderItemService.GetAllOrderItemAsync();
                var orderItemDto = _mapper.Map<IEnumerable<OrderItemDto>>(orderItem);
                return Ok(orderItemDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderItemById(int id)
        {
            try
            {
                var orderItem = await _orderItemService.GetOrderItemByIdAsync(id);
                if (orderItem == null)
                {
                    return NotFound();
                }

                var orderItemDto = _mapper.Map<OrderItemDto>(orderItem);
                return Ok(orderItemDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddOrderItem(OrderItemCreateDto orderItemDto)
        {
            try
            {
                if (orderItemDto == null)
                {
                    return BadRequest("OrderItem is null");
                }
                if (await _orderItemService.GetOrderItemByIdAsync(orderItemDto.Id) != null)
                {
                    return BadRequest("Order already exists");
                }

                var order = await _orderService.GetOrderByIdAsync(orderItemDto.OrderId);
                if (order == null)
                {
                    return BadRequest("Order not found");
                }

                Product product = await _productService.GetProducttByIdAsync(orderItemDto.ProductId);
                if (product == null)
                {
                    return BadRequest("Product not found");
                }
                if (product.Stock < orderItemDto.Quantity)
                {
                    return BadRequest("Product quantity is not enough");
                }

                var orderItem = _mapper.Map<OrderItem>(orderItemDto);

                orderItem.UnitPrice= product.Price* orderItemDto.Quantity;
                product.Stock -= orderItemDto.Quantity;

                order.TotalAmount += orderItem.UnitPrice;

                await _orderItemService.AddOrderItemAsync(orderItem);
                await _productService.UpdateProductAsync(product);
                await _orderService.UpdateOrderAsync(order);

                return CreatedAtAction(nameof(GetOrderItemById), new { id = orderItem.Id }, orderItem);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrderItem(int id, OrderItemCreateDto orderItemDto)
        {
            //try
            //{
                if (orderItemDto == null || id != orderItemDto.Id)
                {
                    return BadRequest("OrderItem is null");
                }
                if(await _orderItemService.GetOrderItemAsync(orderItemDto.Id) == null)
                {
                    return BadRequest("OrderItem not found");
                }

                Order order = await _orderService.GetOrderByIdAsync(orderItemDto.OrderId);
                if (order == null)
                {
                    return BadRequest("Order not found");
                }

                Product product = await _productService.GetProducttByIdAsync(orderItemDto.ProductId);
                if (product == null)
                {
                    return BadRequest("Product not found");
                }

                OrderItem orderItem= await _orderItemService.GetOrderItemAsync(orderItemDto.Id); 
                if(orderItem.ProductId != orderItemDto.ProductId)
                {
                    if (product.Stock < orderItemDto.Quantity)
                    {
                        return BadRequest("Product quantity is not enough");
                    }
                    product.Stock -= orderItemDto.Quantity;
                    await _productService.UpdateProductAsync(product);
                    Product productOld = await _productService.GetProducttByIdAsync(orderItem.ProductId); 
                    productOld.Stock += orderItem.Quantity;
                    await _productService.UpdateProductAsync(productOld);
                }
                else
                {
                    if (product.Stock < orderItemDto.Quantity - orderItem.Quantity)
                    {
                        return BadRequest("Product quantity is not enough");
                    }
                    product.Stock -= orderItemDto.Quantity - orderItem.Quantity;
                    await _productService.UpdateProductAsync(product);
                }

               
                var orderItemMapper = _mapper.Map<OrderItem>(orderItemDto);
                orderItemMapper.UnitPrice = product.Price * orderItemDto.Quantity;
                await _orderItemService.UpdateOrderItemAsync(orderItemMapper);
            order.TotalAmount = order.TotalAmount - orderItem.UnitPrice + product.Price * orderItemDto.Quantity;
            await _orderService.UpdateOrderAsync(order);
            return Ok();
            //}
            //catch (Exception ex)
            //{
            //    return BadRequest(ex.Message);
            //}
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderItem(int id)
        {
            try
            {
                var orderItem = await _orderItemService.GetOrderItemByIdAsync(id);
                if (orderItem == null)
                {
                    return NotFound();
                }

                Order order = await _orderService.GetOrderByIdAsync(orderItem.OrderId);
                Product product = await _productService.GetProducttByIdAsync(orderItem.ProductId);

                order.TotalAmount -= orderItem.UnitPrice;
                product.Stock += orderItem.Quantity;

                await _orderService.UpdateOrderAsync(order);
                await _productService.UpdateProductAsync(product);
                await _orderItemService.DeleteOrderItemAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
