﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Dto;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;

namespace PFR_NEFC_KS_Practice_T01.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProduct()
        {
            try
            {
                var product= await _productService.GetAllProductAsync();
                var coursesDto = _mapper.Map<IEnumerable<ProductDto>>(product);
                return Ok(coursesDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductById(int id)
        {
            try
            {
                var course = await _productService.GetProducttByIdAsync(id);
                if (course == null)
                {
                    return NotFound();
                }

                var ProductDto = _mapper.Map<ProductDto>(course);
                return Ok(ProductDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Addproduct(ProductDto productDto)
        {
            try
            {
                if (productDto == null)
                {
                    return BadRequest("Product is null");
                }
                if (await _productService.GetProducttByIdAsync(productDto.Id) != null)
                {
                    return BadRequest("Product already exists");
                }
                var course = _mapper.Map<Product>(productDto);
                await _productService.AddProductAsync(course);
                return CreatedAtAction(nameof(GetProductById), new { id = course.Id }, course);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCourse(int id, ProductDto ProductDto)
        {
            try
            {
                if (ProductDto == null || id != ProductDto.Id)
                {
                    return BadRequest("Product is null");
                }

                if (await _productService.GetProducttByIdAsync(id) == null)
                {
                    return NotFound();
                }

                var course = _mapper.Map<Product>(ProductDto);
                await _productService.UpdateProductAsync(course);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCourse(int id)
        {
            try
            {
                if (await _productService.GetProducttByIdAsync(id) == null)
                {
                    return NotFound();
                }
                await _productService.DeleteProductAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
