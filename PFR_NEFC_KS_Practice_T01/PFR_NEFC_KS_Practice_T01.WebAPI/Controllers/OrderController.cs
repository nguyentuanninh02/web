﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Dto;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Service;
using PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;

namespace PFR_NEFC_KS_Practice_T01.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;

        public OrderController(IOrderService orderService, IMapper mapper)
        {
            _orderService = orderService;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrder()
        {
            try
            {
                var Order = await _orderService.GetAllOrderAsync();
                var coursesDto = _mapper.Map<IEnumerable<OrderDto>>(Order);
                return Ok(coursesDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderById(int id)
        {
            try
            {
                var course = await _orderService.GetOrderByIdAsync(id);
                if (course == null)
                {
                    return NotFound();
                }

                var OrderDto = _mapper.Map<OrderDto>(course);
                return Ok(OrderDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddOrder(OrderCreateDto oderCreateDto)
        {
            try
            {
                if (oderCreateDto == null)
                {
                    return BadRequest("Order is null");
                }
                if (await _orderService.GetOrderByIdAsync(oderCreateDto.Id) != null)
                {
                    return BadRequest("Order already exists");
                }
                var course = _mapper.Map<Order>(oderCreateDto);
                await _orderService.AddOrderAsync(course);
                return CreatedAtAction(nameof(GetOrderById), new { id = course.Id }, course);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrder(int id, OrderCreateDto orderCreateDto)
        {
            try
            {
                if (orderCreateDto == null || id != orderCreateDto.Id)
                {
                    return BadRequest("Order is null");
                }

                if(await _orderService.GetOrderByIdAsync(id) == null)
                {
                    return NotFound();
                }

                var course = _mapper.Map<Order>(orderCreateDto);
                await _orderService.UpdateOrderAsync(course);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            try
            {
                if (await _orderService.GetOrderByIdAsync(id) == null)
                {
                    return NotFound();
                }
                await _orderService.DeleteOrderAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

