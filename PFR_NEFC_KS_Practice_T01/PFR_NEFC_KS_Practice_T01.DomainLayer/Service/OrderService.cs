﻿using PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.DomainLayer.Service
{
    public class OrderService: IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddOrderAsync(Order order)
        {
            await _unitOfWork.Order.AddAsync(order);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteOrderAsync(int id)
        {
            var course = await _unitOfWork.Order.GetByIdAsync(id);
            if (course == null)
            {
                return;
            }
            await _unitOfWork.Order.RemoveAsync(course);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<Order>> GetAllOrderAsync()
        {
            return await _unitOfWork.Order.GetAllAsync();
        }

        public async Task<Order?> GetOrderByIdAsync(int id)
        {
            return await _unitOfWork.Order.GetByIdAsync(id);
        }

        public async Task UpdateOrderAsync(Order order)
        {
            await _unitOfWork.Order.UpdateAsync(order);
            await _unitOfWork.SaveChange();
        }
    }
}
