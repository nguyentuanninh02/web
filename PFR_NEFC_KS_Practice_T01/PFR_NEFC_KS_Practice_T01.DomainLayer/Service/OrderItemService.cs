﻿using PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.DomainLayer.Service
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderItemService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddOrderItemAsync(OrderItem orderItem)
        {
            await _unitOfWork.OrderItem.AddAsync(orderItem);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteOrderItemAsync(int id)
        {
            var course = await _unitOfWork.OrderItem.GetByIdAsync(id);
            if (course == null)
            {
                return;
            }
            await _unitOfWork.OrderItem.RemoveAsync(course);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<OrderItem>> GetAllOrderItemAsync()
        {
            return await _unitOfWork.OrderItem.GetAllAsync();
        }

        public async Task<OrderItem?> GetOrderItemByIdAsync(int id)
        {
            return await _unitOfWork.OrderItem.GetByIdAsync(id);
        }

        public async Task UpdateOrderItemAsync(OrderItem orderItem)
        {
            await _unitOfWork.OrderItem.UpdateAsync(orderItem);
            await _unitOfWork.SaveChange();
        }

        public async Task<OrderItem> GetOrderItemAsync(int id)
        {
            return await _unitOfWork.OrderItem.GetAsync(x => x.Id == id, tracked: false);
        }
    }
}
