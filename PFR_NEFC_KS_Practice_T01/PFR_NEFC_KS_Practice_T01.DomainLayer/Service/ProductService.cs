﻿using PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.DomainLayer.Service
{
    public class ProductService: IProductService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddProductAsync(Product role)
        {
            await _unitOfWork.Product.AddAsync(role);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteProductAsync(int id)
        {
            var course = await _unitOfWork.Product.GetByIdAsync(id);
            if (course == null)
            {
                return;
            }
            await _unitOfWork.Product.RemoveAsync(course);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<Product>> GetAllProductAsync()
        {
            return await _unitOfWork.Product.GetAllAsync();
        }

        public async Task<Product?> GetProducttByIdAsync(int id)
        {
            return await _unitOfWork.Product.GetByIdAsync(id);
        }

        public async Task UpdateProductAsync(Product role)
        {
            await _unitOfWork.Product.UpdateAsync(role);
            await _unitOfWork.SaveChange();
        }
    }
}
