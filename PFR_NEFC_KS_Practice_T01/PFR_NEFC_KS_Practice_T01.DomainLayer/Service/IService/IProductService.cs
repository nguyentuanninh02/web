﻿using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.DomainLayer.Service.IService
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAllProductAsync();
        Task<Product?> GetProducttByIdAsync(int id);
        Task AddProductAsync(Product role);
        Task UpdateProductAsync(Product role);
        Task DeleteProductAsync(int id);
    }
}
