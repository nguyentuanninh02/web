﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.DomainLayer.Dto
{
    public class OrderCreateDto
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
    }
}
