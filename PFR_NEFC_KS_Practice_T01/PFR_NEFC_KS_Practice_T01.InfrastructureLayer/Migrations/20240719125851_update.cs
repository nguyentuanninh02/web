﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Migrations
{
    /// <inheritdoc />
    public partial class update : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Order",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2024, 7, 19, 19, 58, 50, 584, DateTimeKind.Local).AddTicks(8197),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Order",
                type: "datetime(6)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2024, 7, 19, 19, 58, 50, 584, DateTimeKind.Local).AddTicks(8197));
        }
    }
}
