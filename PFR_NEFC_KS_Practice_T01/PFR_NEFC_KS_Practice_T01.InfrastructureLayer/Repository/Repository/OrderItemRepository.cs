﻿using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Context;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.Repository
{
    public class OrderItemRepository : Repository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(AppDbContext context) : base(context)
        {

        }
    
    }
}
