﻿using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.IRepository
{
    public interface IOrderItemRepository: IRepository<OrderItem>
    {
    }
}
