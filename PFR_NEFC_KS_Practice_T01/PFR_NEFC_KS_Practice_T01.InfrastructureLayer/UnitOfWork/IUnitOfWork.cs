﻿using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.InfrastructureLayer.UnitOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        IProductRepository Product { get; }
        IOrderRepository Order { get; }
        IOrderItemRepository OrderItem { get; }
        Task<int> SaveChange();
    }
}
