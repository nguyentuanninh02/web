﻿using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Context;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.IRepository;
using PFR_NEFC_KS_Practice_T01.InfrastructureLayer.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_NEFC_KS_Practice_T01.InfrastructureLayer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public IProductRepository Product { get; private set; }
        public IOrderRepository Order { get; private set; }
        public IOrderItemRepository OrderItem { get; private set; }

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            Product = new ProductRepository(_context);
            Order = new OrderRepository(_context);
            OrderItem = new OrderItemRepository(_context);
        }
        public void Dispose()
        {
            _context.Dispose();
        }

        public Task<int> SaveChange()
        {
            return _context.SaveChangesAsync();
        }
    }
}
