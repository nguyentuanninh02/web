﻿using Microsoft.EntityFrameworkCore;
using NEFW.KS.L001.Infastructor.Config;
using NEFW.KS.L001.Infastructor.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Infastructor.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OrderConfig).Assembly);
            modelBuilder.Entity<Customer>().HasData(
                               new Customer
                               {
                                   Id = 1,
                                   Name = "Customer 1",
                                   Email = ""
                               },
                               new Customer
                               {
                                   Id = 2,
                                   Name = "Customer 2",
                                   Email = ""
                               },
                               new Customer
                               {
                                   Id = 3,
                                   Name = "Customer 3",
                                   Email = ""
                               }
                               );
        }
    }
}
