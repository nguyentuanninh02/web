﻿using NEFW.KS.L001.Infastructor.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Infastructor.UnitOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        IOrderItemRepository OrderItem{ get; }
        IOrderRepository Order{ get; }
        ICustomerRepository Customer{ get; }
        IProductRepository Product { get; }
        Task<int> SaveChange();
    }
}
