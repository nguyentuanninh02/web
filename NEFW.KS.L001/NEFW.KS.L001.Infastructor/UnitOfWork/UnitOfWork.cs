﻿using NEFW.KS.L001.Infastructor.Context;
using NEFW.KS.L001.Infastructor.Repository.IRepository;
using NEFW.KS.L001.Infastructor.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Infastructor.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Order = new OrderRepository(_context);
            OrderItem = new OrderItemRepository(_context);
            Customer = new CustomerRepository(_context);
            Product = new ProductRepository(_context);
        }
        public IOrderRepository Order { get; private set; }
        public IOrderItemRepository OrderItem { get; private set; }
        public ICustomerRepository Customer { get; private set; }
        public IProductRepository Product { get; private set; }
        public Task<int> SaveChange()
        {
            return _context.SaveChangesAsync();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
