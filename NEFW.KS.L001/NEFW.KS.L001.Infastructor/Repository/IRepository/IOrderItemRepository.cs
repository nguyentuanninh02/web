﻿using NEFW.KS.L001.Infastructor.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Infastructor.Repository.IRepository
{
    public interface IOrderItemRepository: IGenericRepository<OrderItem>
    {
        Task<List<OrderItem>> GetAllWithAllInfo();
        Task<OrderItem> GetWithAllInfo(int id);
    }
}
