﻿using NEFW.KS.L001.Infastructor.Context;
using NEFW.KS.L001.Infastructor.Entity;
using NEFW.KS.L001.Infastructor.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Infastructor.Repository.Repository
{
    public class CustomerRepository: GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ApplicationDbContext context): base(context)
        {
        }
    }
}
