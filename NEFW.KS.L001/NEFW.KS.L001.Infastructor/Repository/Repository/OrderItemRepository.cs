﻿using Microsoft.EntityFrameworkCore;
using NEFW.KS.L001.Infastructor.Context;
using NEFW.KS.L001.Infastructor.Entity;
using NEFW.KS.L001.Infastructor.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Infastructor.Repository.Repository
{
    public class OrderItemRepository : GenericRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<OrderItem>> GetAllWithAllInfo()
        {
            return await _context.Set<OrderItem>().Include(x => x.Product).Include(x => x.Order).ThenInclude(x=> x.Customer).ToListAsync();
        }


        public async Task<OrderItem> GetWithAllInfo(int id)
        {
            return await _context.Set<OrderItem>().Include(x => x.Product).Include(x => x.Order).ThenInclude(x => x.Customer).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
