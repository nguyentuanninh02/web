﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Infastructor.Entity
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        public Order() { }

        public Order(int id, DateTime orderDate, int customerId, Customer customer)
        {
            Id = id;
            OrderDate = orderDate;
            CustomerId = customerId;
            Customer = customer;
        }
    }
}
