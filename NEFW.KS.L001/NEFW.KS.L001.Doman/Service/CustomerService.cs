﻿using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;
using NEFW.KS.L001.Infastructor.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Doman.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CreateCustomerAsync(Customer customer)
        {
            await _unitOfWork.Customer.CreateAsync(customer);
            await _unitOfWork.SaveChange();

        }

        public async Task DeleteCustomerAsync(int id)
        {
            var customer = await _unitOfWork.Customer.GetAsync(x => x.Id == id);
            if (customer == null)
            {
                return;
            }
            await _unitOfWork.Customer.RemoveAsync(customer);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<Customer>> GetAllCustomerAsync()
        {
            return await _unitOfWork.Customer.GetAllAsync();
        }

        public async Task<Customer?> GetCustomerByIdAsync(int id)
        {
            return await _unitOfWork.Customer.GetAsync(x => x.Id == id);
        }

        public async Task UpdateCustomerAsync(Customer customer)
        {
            await _unitOfWork.Customer.UpdateAsync(customer);
            await _unitOfWork.SaveChange();
        }
    }
}
