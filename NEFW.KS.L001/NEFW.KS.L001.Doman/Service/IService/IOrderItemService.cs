﻿using NEFW.KS.L001.Infastructor.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Doman.Service.IService
{
    public interface IOrderItemService
    {
        Task<IEnumerable<OrderItem>> GetAllOrderItemAsync(Expression<Func<OrderItem, bool>>? filter = null, string? includeProperties = null);
        Task<OrderItem?> GetOrderItemByIdAsync(int id, bool tracked = true, string? includeProperties = null);
        Task<IEnumerable<OrderItem>> GetAllOrderItemWithAllInfo();
        Task<OrderItem?> GetOrderItemByIdWithAllInfo(int id);
        Task CreateOrderItemAsync(OrderItem orderItem);
        Task UpdateOrderItemAsync(OrderItem orderItem);
        Task DeleteOrderItemAsync(int id);
    }
}
