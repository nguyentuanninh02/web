﻿using NEFW.KS.L001.Infastructor.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Doman.Service.IService
{
    public interface IOrderService
    {
        Task<IEnumerable<Order>> GetAllOrderAsync(Expression<Func<Order, bool>>? filter = null, string? includeProperties = null);
        Task<Order?> GetOrderByIdAsync(int id, bool tracked = true, string? includeProperties = null);
        Task CreateOrderAsync(Order order1);
        Task UpdateOrderAsync(Order order);
        Task DeleteOrderAsync(int id);
    }
}
