﻿using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;
using NEFW.KS.L001.Infastructor.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Doman.Service
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CreateOrderAsync(Order order1)
        {
            await _unitOfWork.Order.CreateAsync(order1);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteOrderAsync(int id)
        {
            var order = await _unitOfWork.Order.GetAsync(x => x.Id == id);
            if (order == null)
            {
                return;
            }
            await _unitOfWork.Order.RemoveAsync(order);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<Order>> GetAllOrderAsync(Expression<Func<Order, bool>>? filter = null, string? includeProperties = null)
        {
            return await _unitOfWork.Order.GetAllAsync(filter, includeProperties);
        }

        public async Task<Order?> GetOrderByIdAsync(int id, bool tracked = true, string? includeProperties = null)
        {
            return await _unitOfWork.Order.GetAsync(x => x.Id == id, tracked, includeProperties);
        }

        public async Task UpdateOrderAsync(Order order)
        {
            await _unitOfWork.Order.UpdateAsync(order);
            await _unitOfWork.SaveChange();
        }
    }
}
