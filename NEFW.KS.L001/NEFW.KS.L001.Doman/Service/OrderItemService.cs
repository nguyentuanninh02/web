﻿using Microsoft.VisualBasic;
using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;
using NEFW.KS.L001.Infastructor.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Doman.Service
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderItemService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CreateOrderItemAsync(OrderItem order1)
        {
            await _unitOfWork.OrderItem.CreateAsync(order1);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteOrderItemAsync(int id)
        {
            var order = await _unitOfWork.OrderItem.GetAsync(x => x.Id == id);
            if (order == null)
            {
                return;
            }
            await _unitOfWork.OrderItem.RemoveAsync(order);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<OrderItem>> GetAllOrderItemAsync(Expression<Func<OrderItem, bool>>? filter = null, string? includeProperties = null)
        {
            return await _unitOfWork.OrderItem.GetAllAsync(filter, includeProperties);
        }

        public async Task<IEnumerable<OrderItem>> GetAllOrderItemWithAllInfo()
        {
            return await _unitOfWork.OrderItem.GetAllWithAllInfo();
        }


        public async Task<OrderItem?> GetOrderItemByIdAsync(int id, bool tracked = true, string? includeProperties = null)
        {
            return await _unitOfWork.OrderItem.GetAsync(x => x.Id == id, tracked, includeProperties);
        }

        public async Task<OrderItem?> GetOrderItemByIdWithAllInfo(int id)
        {
            return await _unitOfWork.OrderItem.GetWithAllInfo(id);
        }

        public async Task UpdateOrderItemAsync(OrderItem order)
        {
            await _unitOfWork.OrderItem.UpdateAsync(order);
            await _unitOfWork.SaveChange();
        }
    }
}
