﻿using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;
using NEFW.KS.L001.Infastructor.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Doman.Service
{
    public class ProductService: IProductService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CreateProductAsync(Product order1)
        {
            await _unitOfWork.Product.CreateAsync(order1);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteProductAsync(int id)
        {
            var order = await _unitOfWork.Product.GetAsync(x => x.Id == id);
            if (order == null)
            {
                return;
            }
            await _unitOfWork.Product.RemoveAsync(order);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<Product>> GetAllProductAsync()
        {
            return await _unitOfWork.Product.GetAllAsync();
        }

        public async Task<Product?> GetProductByIdAsync(int id)
        {
            return await _unitOfWork.Product.GetAsync(x => x.Id == id);
        }

        public async Task UpdateProductAsync(Product order)
        {
            await _unitOfWork.Product.UpdateAsync(order);
            await _unitOfWork.SaveChange();
        }
    }
}
