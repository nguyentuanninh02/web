﻿using AutoMapper;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.Infastructor.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFW.KS.L001.Doman.Mapping
{
    public class MappingConfig: Profile
    {
        public MappingConfig()
        {
            CreateMap<ProductDto, Product>().ReverseMap();   
            CreateMap<ProductCreateDto, Product>().ReverseMap();

            CreateMap<CustomerDto, Customer>().ReverseMap();
            CreateMap<CustomerCreateDto, Customer>().ReverseMap();

            CreateMap<OrderDto, Order>().ReverseMap();
            CreateMap<OrderCreateDto, Order>().ReverseMap();
            CreateMap<OrderUpdateDto, Order>().ReverseMap();

            CreateMap<OrderItemDto, OrderItem>().ReverseMap();
            CreateMap<OrderItemCreateDto, OrderItem>().ReverseMap();
            CreateMap<OrderItemUpdateDto, OrderItem>().ReverseMap();

        }

    }
}
