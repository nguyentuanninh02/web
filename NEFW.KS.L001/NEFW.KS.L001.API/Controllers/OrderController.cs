﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;

namespace NEFW.KS.L001.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;
        protected TableResponse<OrderDto> _response;

        public OrderController(IOrderService orderService, ICustomerService customerService , IMapper mapper)
        {
            _orderService = orderService;
            _customerService= customerService;
            _mapper = mapper;
            _response = new();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrder()
        {
            try
            {
                var order = await _orderService.GetAllOrderAsync(includeProperties:"Customer");
                var orderDto = _mapper.Map<IEnumerable<OrderDto>>(order);
                _response.Data = orderDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Code = 500;
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderById(int id)
        {
            try
            {
                var order = await _orderService.GetOrderByIdAsync(id, includeProperties:"Customer");
                if (order == null)
                {
                    _response.Code = 404;
                    return NotFound(_response);
                }
                var orderDto = _mapper.Map<OrderDto>(order);
                _response.Data = orderDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder(OrderCreateDto orderDto)
        {
            try
            {
                if (orderDto == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                if(await _customerService.GetCustomerByIdAsync(orderDto.CustomerId) == null)
                {
                    _response.Success = false;
                    _response.Code = 404;
                    _response.Message = "Customer not found";
                    return NotFound(_response);
                }
                var order = _mapper.Map<Order>(orderDto);
                await _orderService.CreateOrderAsync(order);
                _response.Code = 201;
                _response.Data = order;
                return CreatedAtAction("GetOrderById", new { id = order.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrder(int id, OrderUpdateDto orderDto)
        {
            try
            {
                if (orderDto == null || id != orderDto.Id)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                if (await _customerService.GetCustomerByIdAsync(orderDto.CustomerId) == null)
                {
                    _response.Success = false;
                    _response.Code = 404;
                    _response.Message = "Customer not found";
                    return NotFound(_response);
                }
                var order = _mapper.Map<Order>(orderDto);
                await _orderService.UpdateOrderAsync(order);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            try
            {
                var order = await _orderService.GetOrderByIdAsync(id);
                if (order == null)
                {
                    _response.Success = false;
                    _response.Code = 404;
                    return NotFound(_response);
                }
                await _orderService.DeleteOrderAsync(id);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }
    }
}
