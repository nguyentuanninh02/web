﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;

namespace NEFW.KS.L001.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;
        protected TableResponse<CustomerDto> _response;

        public CustomerController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
            _response = new();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCustomer()
        {
            try
            {
                var customer = await _customerService.GetAllCustomerAsync();
                var customerDto = _mapper.Map<IEnumerable<CustomerDto>>(customer);
                _response.Data = customerDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Code = 500;
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomerById(int id)
        {
            try
            {
                var customer = await _customerService.GetCustomerByIdAsync(id);
                if (customer == null)
                {
                    _response.Code = 404;
                    return NotFound(_response);
                }
                var customerDto = _mapper.Map<CustomerDto>(customer);
                _response.Data = customerDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCustomer(CustomerCreateDto customerDto)
        {
            try
            {
                if (customerDto == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                var customer = _mapper.Map<Customer>(customerDto);
                await _customerService.CreateCustomerAsync(customer);
                _response.Code = 201;
                _response.Data = customer;
                return CreatedAtAction("GetCustomerById", new { id = customer.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomer(int id, CustomerDto customerDto)
        {
            try
            {
                if (customerDto == null || id != customerDto.Id)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                var customer = _mapper.Map<Customer>(customerDto);
                await _customerService.UpdateCustomerAsync(customer);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            try
            {
                var customer = await _customerService.GetCustomerByIdAsync(id);
                if (customer == null)
                {
                    _response.Success = false;
                    _response.Code = 404;
                    return NotFound(_response);
                }
                await _customerService.DeleteCustomerAsync(id);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }
    }
}
