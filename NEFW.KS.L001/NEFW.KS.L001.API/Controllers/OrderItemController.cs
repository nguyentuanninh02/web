﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;

namespace NEFW.KS.L001.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItemController : ControllerBase
    {
        private readonly IOrderItemService _orderItemService;
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        protected TableResponse<OrderItemDto> _response;

        public OrderItemController(IOrderItemService orderItemService, IOrderService orderService, IProductService productService, IMapper mapper)
        {
            _orderItemService = orderItemService;
            _orderService = orderService;
            _productService = productService;
            _mapper = mapper;
            _response = new();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrderItem()
        {
            try
            {
                var orderItem = await _orderItemService.GetAllOrderItemWithAllInfo();
                var orderItemDto = _mapper.Map<IEnumerable<OrderItemDto>>(orderItem);
                _response.Data = orderItemDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderItemById(int id)
        {
            try
            {
                var orderItem = await _orderItemService.GetOrderItemByIdWithAllInfo(id);
                if (orderItem == null)
                {
                    _response.Code = 404;
                    return NotFound(_response);
                }
                var orderItemDto = _mapper.Map<OrderItemDto>(orderItem);
                _response.Data = orderItemDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrderItem(OrderItemCreateDto orderItemDto)
        {
            try
            {
                if (orderItemDto == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                if(orderItemDto.Quantity <= 0)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    _response.DataError = "Quantity must be greater than 0";
                    return BadRequest(_response);
                }
                if(await _orderService.GetOrderByIdAsync(orderItemDto.OrderId) == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    _response.DataError = "Order not found";
                    return BadRequest(_response);
                }
                if(await _productService.GetProductByIdAsync(orderItemDto.ProductId) == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    _response.DataError = "Product not found";
                    return BadRequest(_response);
                }
                
                var orderItem = _mapper.Map<OrderItem>(orderItemDto);
                await _orderItemService.CreateOrderItemAsync(orderItem);
                _response.Code = 201;
                _response.Data = orderItem;
                return CreatedAtAction(nameof(GetOrderItemById), new { id = orderItem.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrderItem(int id, OrderItemUpdateDto orderItemDto)
        {
            try
            {
                if (orderItemDto == null || id != orderItemDto.Id)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                if (await _orderService.GetOrderByIdAsync(orderItemDto.OrderId) == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    _response.DataError = "Order not found";
                    return BadRequest(_response);
                }
                if (await _productService.GetProductByIdAsync(orderItemDto.ProductId) == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    _response.DataError = "Product not found";
                    return BadRequest(_response);
                }
                var orderItem = _mapper.Map<OrderItem>(orderItemDto);
                await _orderItemService.UpdateOrderItemAsync(orderItem);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderItem(int id)
        {
            try
            {
                var orderItem = await _orderItemService.GetOrderItemByIdAsync(id);
                if (orderItem == null)
                {
                    _response.Success = false;
                    _response.Code = 404;
                    return NotFound(_response);
                }
                await _orderItemService.DeleteOrderItemAsync(id);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }
    }
}
