﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.Doman.Service.IService;
using NEFW.KS.L001.Infastructor.Entity;
using System.Net;

namespace NEFW.KS.L001.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        protected TableResponse<ProductDto> _response;

        public ProductController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
            _response = new();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProduct()
        {
            try
            {
                var product = await _productService.GetAllProductAsync();
                var productDto = _mapper.Map<IEnumerable<ProductDto>>(product);
                _response.Data = productDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Code = 500;
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductById(int id)
        {
            try
            {
                var product = await _productService.GetProductByIdAsync(id);
                if (product == null)
                {
                    _response.Code = 404;
                    return NotFound(_response);
                }
                var productDto = _mapper.Map<ProductDto>(product);
                _response.Data = productDto;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductCreateDto productDto)
        {
            try
            {
                if (productDto == null)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                var product = _mapper.Map<Product>(productDto);
                await _productService.CreateProductAsync(product);
                _response.Code = 201;
                _response.Data = product;
                return CreatedAtAction("GetProductById", new { id = product.Id }, _response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProduct(int id, ProductDto productDto)
        {
            try
            {
                if (productDto == null || id != productDto.Id)
                {
                    _response.Success = false;
                    _response.Code = 400;
                    return BadRequest(_response);
                }
                var product = _mapper.Map<Product>(productDto);
                await _productService.UpdateProductAsync(product);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            try
            {
                var product = await _productService.GetProductByIdAsync(id);
                if (product == null)
                {
                    _response.Success = false;
                    _response.Code = 404;
                    return NotFound(_response);
                }
                await _productService.DeleteProductAsync(id);
                _response.Code = 204;
                return Ok(_response);
            }
            catch (Exception ex)
            {
                _response.Success = false;
                _response.DataError = ex.Message;
                return BadRequest(_response);
            }
        }
    }
}
