﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.WebApp.Models;
using Newtonsoft.Json;
using System.Text;

namespace NEFW.KS.L001.WebApp.Controllers
{
    public class OrderController : Controller
    {
        public async Task<IActionResult> Index()
        {
            var tableResponse = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Order"
            });

            List<OrderDto> orderDtos = new List<OrderDto>();
            if (tableResponse.Success)
            {
                orderDtos = JsonConvert.DeserializeObject<List<OrderDto>>(Convert.ToString(tableResponse.Data));
            }

            return View(orderDtos);
        }

        public async Task<IActionResult> Create()
        {
            var listCustomer = await SendAsync<CustomerDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Customer"
            });
            List<CustomerDto> customers= JsonConvert.DeserializeObject<List<CustomerDto>>(Convert.ToString(listCustomer.Data));

            ViewData["CustomerId"] = customers.Select(x=> new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] OrderCreateDto orderDto)
        {
            if (!ModelState.IsValid)
            {
                return View(orderDto);
            }

            var tableResponse = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.POST,
                Url = "https://localhost:7206/api/Order",
                Data = orderDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            var listCustomer = await SendAsync<CustomerDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Customer"
            });
            List<CustomerDto> customers = JsonConvert.DeserializeObject<List<CustomerDto>>(Convert.ToString(listCustomer.Data));

            ViewData["CustomerId"] = customers.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            return View(orderDto);
        }

        public async Task<IActionResult> Update(int id)
        {
            var tableResponse = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/Order/{id}"
            });

            OrderUpdateDto orderDto = new OrderUpdateDto();
            if (tableResponse.Success)
            {
                orderDto = JsonConvert.DeserializeObject<OrderUpdateDto>(Convert.ToString(tableResponse.Data));
            }

            var listCustomer = await SendAsync<CustomerDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Customer"
            });
            List<CustomerDto> customers = JsonConvert.DeserializeObject<List<CustomerDto>>(Convert.ToString(listCustomer.Data));

            ViewData["CustomerId"] = customers.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });

            return View(orderDto);
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] OrderUpdateDto orderDto)
        {
            if (!ModelState.IsValid)
            {
                return View(orderDto);
            }

            var tableResponse = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.PUT,
                Url = $"https://localhost:7206/api/Order/{orderDto.Id}",
                Data = orderDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            var listCustomer = await SendAsync<CustomerDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Customer"
            });
            List<CustomerDto> customers = JsonConvert.DeserializeObject<List<CustomerDto>>(Convert.ToString(listCustomer.Data));

            ViewData["CustomerId"] = customers.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            return View(orderDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var tableResponse = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/Order/{id}"
            });

            OrderDto orderDto = new OrderDto();
            if (tableResponse.Success)
            {
                orderDto = JsonConvert.DeserializeObject<OrderDto>(Convert.ToString(tableResponse.Data));
            }

            return View(orderDto);
        }

        [HttpPost]
        public async Task<IActionResult> Delete([FromBody] OrderDto orderDto)
        {
            var tableResponse = await SendAsync<OrderDto>(new APIRequest
            {   
                ApiType = SD.ApiType.DELETE,
                Url = $"https://localhost:7206/api/Order/{orderDto.Id}"
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(orderDto);
        }

        public async Task<TableResponse<T>> SendAsync<T>(APIRequest apiRequest)
        {
            try
            {
                var client = new HttpClient();
                HttpRequestMessage message = new HttpRequestMessage();
                message.Headers.Add("Accept", "application/json");
                message.RequestUri = new Uri(apiRequest.Url);
                if (apiRequest.Data != null)
                {
                    message.Content = new StringContent(JsonConvert.SerializeObject(apiRequest.Data),
                        Encoding.UTF8, "application/json");
                }
                switch (apiRequest.ApiType)
                {
                    case SD.ApiType.POST:
                        message.Method = HttpMethod.Post;
                        break;
                    case SD.ApiType.PUT:
                        message.Method = HttpMethod.Put;
                        break;
                    case SD.ApiType.DELETE:
                        message.Method = HttpMethod.Delete;
                        break;
                    default:
                        message.Method = HttpMethod.Get;
                        break;

                }

                HttpResponseMessage apiResponse = null;

                apiResponse = await client.SendAsync(message);

                TableResponse<T> tableResponse = new TableResponse<T>();

                if (apiResponse.IsSuccessStatusCode)
                {
                    string jsonResponse = await apiResponse.Content.ReadAsStringAsync();
                    tableResponse = JsonConvert.DeserializeObject<TableResponse<T>>(jsonResponse);
                }
                else
                {
                    tableResponse.Code = (int)apiResponse.StatusCode;
                    tableResponse.Success = false;
                    tableResponse.Message = "failed";
                }

                return tableResponse;

            }
            catch (Exception e)
            {
                var dto = new TableResponse<T>
                {
                    Message = e.Message,
                    Success = false
                };
                var res = JsonConvert.SerializeObject(dto);
                var APIResponse = JsonConvert.DeserializeObject<TableResponse<T>>(res);
                return APIResponse;
            }
        }
    }
}
