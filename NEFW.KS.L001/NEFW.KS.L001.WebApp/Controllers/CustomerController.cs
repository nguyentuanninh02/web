﻿using Azure;
using Microsoft.AspNetCore.Mvc;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.Infastructor.Entity;
using NEFW.KS.L001.WebApp.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace NEFW.KS.L001.WebApp.Controllers
{
    public class CustomerController : Controller
    {
        public async Task<IActionResult> Index()
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Customer"
            });

            List<CustomerDto> customerDtos = new List<CustomerDto>();
            if (tableResponse.Success)
            {
                customerDtos = JsonConvert.DeserializeObject<List<CustomerDto>>(Convert.ToString(tableResponse.Data));
            }
           
            return View(customerDtos);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CustomerCreateDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                return View(customerDto);
            }

            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.POST,
                Url = "https://localhost:7206/api/Customer",
                Data = customerDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(customerDto);
        }

        public async Task<IActionResult> Update(int id)
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/Customer/{id}"
            });

            CustomerDto customerDto = new CustomerDto();
            if (tableResponse.Success)
            {
                customerDto = JsonConvert.DeserializeObject<CustomerDto>(Convert.ToString(tableResponse.Data));
            }

            return View(customerDto);
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                return View(customerDto);
            }

            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.PUT,
                Url = $"https://localhost:7206/api/Customer/{customerDto.Id}",
                Data = customerDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(customerDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/Customer/{id}"
            });

            CustomerDto customerDto = new CustomerDto();
            if (tableResponse.Success)
            {
                customerDto = JsonConvert.DeserializeObject<CustomerDto>(Convert.ToString(tableResponse.Data));
            }

            return View(customerDto);
        }

        [HttpPost]
        public async Task<IActionResult> Delete([FromBody] CustomerDto customerDto)
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.DELETE,
                Url = $"https://localhost:7206/api/Customer/{customerDto.Id}"
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(customerDto);
        }

        public async Task<TableResponse<CustomerDto>> SendAsync(APIRequest apiRequest)
        {
            try
            {
                var client = new HttpClient();
                HttpRequestMessage message = new HttpRequestMessage();
                message.Headers.Add("Accept", "application/json");
                message.RequestUri = new Uri(apiRequest.Url);
                if (apiRequest.Data != null)
                {
                    message.Content = new StringContent(JsonConvert.SerializeObject(apiRequest.Data),
                        Encoding.UTF8, "application/json");
                }
                switch (apiRequest.ApiType)
                {
                    case SD.ApiType.POST:
                        message.Method = HttpMethod.Post;
                        break;
                    case SD.ApiType.PUT:
                        message.Method = HttpMethod.Put;
                        break;
                    case SD.ApiType.DELETE:
                        message.Method = HttpMethod.Delete;
                        break;
                    default:
                        message.Method = HttpMethod.Get;
                        break;

                }

                HttpResponseMessage apiResponse = null;

                apiResponse = await client.SendAsync(message);

                TableResponse<CustomerDto> tableResponse = new TableResponse<CustomerDto>();

                if (apiResponse.IsSuccessStatusCode)
                {
                    string jsonResponse = await apiResponse.Content.ReadAsStringAsync();
                    tableResponse = JsonConvert.DeserializeObject<TableResponse<CustomerDto>>(jsonResponse);
                }
                else
                {
                    tableResponse.Code = (int)apiResponse.StatusCode;
                    tableResponse.Success = false;
                    tableResponse.Message = "failed";
                }

                return tableResponse;

            }
            catch (Exception e)
            {
                var dto = new TableResponse<CustomerDto>
                {
                    Message = e.Message,
                    Success = false
                };
                var res = JsonConvert.SerializeObject(dto);
                var APIResponse = JsonConvert.DeserializeObject<TableResponse<CustomerDto>>(res);
                return APIResponse;
            }
        }
    }
}
