﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.WebApp.Models;
using Newtonsoft.Json;
using System.Text;

namespace NEFW.KS.L001.WebApp.Controllers
{
    public class OrderItemController : Controller
    {
        public async Task<IActionResult> Index()
        {
            var tableResponse = await SendAsync<OrderItemDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/OrderItem"
            });

            List<OrderItemDto> orderItemDtos = new List<OrderItemDto>();
            if (tableResponse.Success)
            {
                orderItemDtos = JsonConvert.DeserializeObject<List<OrderItemDto>>(Convert.ToString(tableResponse.Data));
            }

            return View(orderItemDtos);
        }

        public async Task<IActionResult> Create()
        {
            var listProduct = await SendAsync<ProductDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Product"
            });
            List<ProductDto> products = JsonConvert.DeserializeObject<List<ProductDto>>(Convert.ToString(listProduct.Data));
            ViewData["ProductId"] = products.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });

            var listOrder = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Order"
            });
            List<OrderDto> orders = JsonConvert.DeserializeObject<List<OrderDto>>(Convert.ToString(listOrder.Data));
            ViewData["OrderId"] = orders.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Id.ToString()
            });

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] OrderItemCreateDto orderItemDto)
        {
            if (!ModelState.IsValid)
            {
                return View(orderItemDto);
            }

            var tableResponse = await SendAsync<OrderItemDto>(new APIRequest
            {
                ApiType = SD.ApiType.POST,
                Url = "https://localhost:7206/api/OrderItem",
                Data = orderItemDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            var listOrder = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Order"
            });
            List<OrderDto> orders = JsonConvert.DeserializeObject<List<OrderDto>>(Convert.ToString(listOrder.Data));
            ViewData["OrderId"] = orders.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Id.ToString()
            });

            var listProduct = await SendAsync<ProductDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Product"
            });
            List<ProductDto> products = JsonConvert.DeserializeObject<List<ProductDto>>(Convert.ToString(listProduct.Data));
            ViewData["ProductId"] = products.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            return View(orderItemDto);
        }

        public async Task<IActionResult> Update(int id)
        {
            var tableResponse = await SendAsync<OrderItemDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/OrderItem/{id}"
            });

            OrderItemUpdateDto orderItemDto = new OrderItemUpdateDto();
            if (tableResponse.Success)
            {
                orderItemDto = JsonConvert.DeserializeObject<OrderItemUpdateDto>(Convert.ToString(tableResponse.Data));
            }

            var listOrder = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Order"
            });
            List<OrderDto> orders = JsonConvert.DeserializeObject<List<OrderDto>>(Convert.ToString(listOrder.Data));
            ViewData["OrderId"] = orders.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Id.ToString()
            });

            var listProduct = await SendAsync<ProductDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Product"
            });
            List<ProductDto> products = JsonConvert.DeserializeObject<List<ProductDto>>(Convert.ToString(listProduct.Data));
            ViewData["ProductId"] = products.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            return View(orderItemDto);
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] OrderItemUpdateDto orderItemDto)
        {
            if (!ModelState.IsValid)
            {
                return View(orderItemDto);
            }

            var tableResponse = await SendAsync<OrderItemDto>(new APIRequest
            {
                ApiType = SD.ApiType.PUT,
                Url = $"https://localhost:7206/api/OrderItem/{orderItemDto.Id}",
                Data = orderItemDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            var listOrder = await SendAsync<OrderDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Order"
            });
            List<OrderDto> orders = JsonConvert.DeserializeObject<List<OrderDto>>(Convert.ToString(listOrder.Data));
            ViewData["OrderId"] = orders.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Id.ToString()
            });

            var listProduct = await SendAsync<ProductDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Product"
            });
            List<ProductDto> products = JsonConvert.DeserializeObject<List<ProductDto>>(Convert.ToString(listProduct.Data));
            ViewData["ProductId"] = products.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            return View(orderItemDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var tableResponse = await SendAsync<OrderItemDto>(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/OrderItem/{id}"
            });

            OrderItemDto orderItemDto = new OrderItemDto();
            if (tableResponse.Success)
            {
                orderItemDto = JsonConvert.DeserializeObject<OrderItemDto>(Convert.ToString(tableResponse.Data));
            }

            return View(orderItemDto);
        }

        [HttpPost]
        public async Task<IActionResult> Delete([FromBody] OrderItemDto orderItemDto)
        {
            var tableResponse = await SendAsync<OrderItemDto>(new APIRequest
            {
                ApiType = SD.ApiType.DELETE,
                Url = $"https://localhost:7206/api/OrderItem/{orderItemDto.Id}"
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(orderItemDto);
        }

        public async Task<TableResponse<T>> SendAsync<T>(APIRequest apiRequest)
        {
            try
            {
                var client = new HttpClient();
                HttpRequestMessage message = new HttpRequestMessage();
                message.Headers.Add("Accept", "application/json");
                message.RequestUri = new Uri(apiRequest.Url);
                if (apiRequest.Data != null)
                {
                    message.Content = new StringContent(JsonConvert.SerializeObject(apiRequest.Data),
                        Encoding.UTF8, "application/json");
                }
                switch (apiRequest.ApiType)
                {
                    case SD.ApiType.POST:
                        message.Method = HttpMethod.Post;
                        break;
                    case SD.ApiType.PUT:
                        message.Method = HttpMethod.Put;
                        break;
                    case SD.ApiType.DELETE:
                        message.Method = HttpMethod.Delete;
                        break;
                    default:
                        message.Method = HttpMethod.Get;
                        break;

                }

                HttpResponseMessage apiResponse = null;

                apiResponse = await client.SendAsync(message);

                TableResponse<T> tableResponse = new TableResponse<T>();

                if (apiResponse.IsSuccessStatusCode)
                {
                    string jsonResponse = await apiResponse.Content.ReadAsStringAsync();
                    tableResponse = JsonConvert.DeserializeObject<TableResponse<T>>(jsonResponse);
                }
                else
                {
                    tableResponse.Code = (int)apiResponse.StatusCode;
                    tableResponse.Success = false;
                    tableResponse.Message = "failed";
                }

                return tableResponse;

            }
            catch (Exception e)
            {
                var dto = new TableResponse<T>
                {
                    Message = e.Message,
                    Success = false
                };
                var res = JsonConvert.SerializeObject(dto);
                var APIResponse = JsonConvert.DeserializeObject<TableResponse<T>>(res);
                return APIResponse;
            }
        }
    }

}
