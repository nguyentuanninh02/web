﻿using Microsoft.AspNetCore.Mvc;
using NEFW.KS.L001.Common;
using NEFW.KS.L001.Doman.Dto;
using NEFW.KS.L001.WebApp.Models;
using Newtonsoft.Json;
using System.Text;

namespace NEFW.KS.L001.WebApp.Controllers
{
    public class ProductController : Controller
    {
        public async Task<IActionResult> Index()
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = "https://localhost:7206/api/Product"
            });

            List<ProductDto> productDtos = new List<ProductDto>();
            if (tableResponse.Success)
            {
                productDtos = JsonConvert.DeserializeObject<List<ProductDto>>(Convert.ToString(tableResponse.Data));
            }

            return View(productDtos);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ProductCreateDto productDto)
        {
            if (!ModelState.IsValid)
            {
                return View(productDto);
            }

            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.POST,
                Url = "https://localhost:7206/api/Product",
                Data = productDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(productDto);
        }

        public async Task<IActionResult> Update(int id)
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/Product/{id}"
            });

            ProductDto productDto = new ProductDto();
            if (tableResponse.Success)
            {
                productDto = JsonConvert.DeserializeObject<ProductDto>(Convert.ToString(tableResponse.Data));
            }

            return View(productDto);
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] ProductDto productDto)
        {
            if (!ModelState.IsValid)
            {
                return View(productDto);
            }

            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.PUT,
                Url = $"https://localhost:7206/api/Product/{productDto.Id}",
                Data = productDto
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(productDto);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.GET,
                Url = $"https://localhost:7206/api/Product/{id}"
            });

            ProductDto productDto = new ProductDto();
            if (tableResponse.Success)
            {
                productDto = JsonConvert.DeserializeObject<ProductDto>(Convert.ToString(tableResponse.Data));
            }

            return View(productDto);
        }

        [HttpPost]
        public async Task<IActionResult> Delete([FromBody] ProductDto productDto)
        {
            var tableResponse = await SendAsync(new APIRequest
            {
                ApiType = SD.ApiType.DELETE,
                Url = $"https://localhost:7206/api/Product/{productDto.Id}"
            });

            if (tableResponse.Success)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(productDto);
        }

        public async Task<TableResponse<ProductDto>> SendAsync(APIRequest apiRequest)
        {
            try
            {
                var client = new HttpClient();
                HttpRequestMessage message = new HttpRequestMessage();
                message.Headers.Add("Accept", "application/json");
                message.RequestUri = new Uri(apiRequest.Url);
                if (apiRequest.Data != null)
                {
                    message.Content = new StringContent(JsonConvert.SerializeObject(apiRequest.Data),
                        Encoding.UTF8, "application/json");
                }
                switch (apiRequest.ApiType)
                {
                    case SD.ApiType.POST:
                        message.Method = HttpMethod.Post;
                        break;
                    case SD.ApiType.PUT:
                        message.Method = HttpMethod.Put;
                        break;
                    case SD.ApiType.DELETE:
                        message.Method = HttpMethod.Delete;
                        break;
                    default:
                        message.Method = HttpMethod.Get;
                        break;

                }

                HttpResponseMessage apiResponse = null;

                apiResponse = await client.SendAsync(message);

                TableResponse<ProductDto> tableResponse = new TableResponse<ProductDto>();

                if (apiResponse.IsSuccessStatusCode)
                {
                    string jsonResponse = await apiResponse.Content.ReadAsStringAsync();
                    tableResponse = JsonConvert.DeserializeObject<TableResponse<ProductDto>>(jsonResponse);
                }
                else
                {
                    tableResponse.Code = (int)apiResponse.StatusCode;
                    tableResponse.Success = false;
                    tableResponse.Message = "failed";
                }

                return tableResponse;

            }
            catch (Exception e)
            {
                var dto = new TableResponse<ProductDto>
                {
                    Message = e.Message,
                    Success = false
                };
                var res = JsonConvert.SerializeObject(dto);
                var APIResponse = JsonConvert.DeserializeObject<TableResponse<ProductDto>>(res);
                return APIResponse;
            }
        }
    }

}
