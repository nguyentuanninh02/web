﻿using NEFW.KS.L001.Common;
using static NEFW.KS.L001.Common.SD;

namespace NEFW.KS.L001.WebApp.Models
{
    public class APIRequest
    {
        public ApiType ApiType { get; set; } = ApiType.GET;
        public string Url { get; set; }
        public object Data { get; set; }
    }
}
