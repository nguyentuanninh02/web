﻿using Infastructure.Entity;
using Infastructure.UnitOfWork;
using KS.Domain.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task AddUserAsync(User user)
        {
            return _unitOfWork.Users.AddAsync(user);
        }

        public async Task DeleteUserAsync(int id)
        {
            var user= await _unitOfWork.Users.GetByIdAsync(id);
            if (user != null)
            {
                await _unitOfWork.Users.RemoveAsync(user);
                await _unitOfWork.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await _unitOfWork.Users.GetAllAsync(includeProperties: "Role");
        }

        public Task<User?> GetUserByIdAsNoTrackingAsync(int id)
        {
            return _unitOfWork.Users.GetUserWithNoTrackingAsync(id); 
        }

        public Task<User?> GetUserByIdAsync(int id)
        {
            return _unitOfWork.Users.GetUserWithRoleByIdAsync(id);
        }

        public async Task UpdateUserAsync(User user)
        {
            await _unitOfWork.Users.UpdateAsync(user);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
