﻿using Infastructure.Entity;
using Infastructure.UnitOfWork;
using KS.Domain.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.Domain.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Role>> GetAllRolesAsync() => await _unitOfWork.Roles.GetAllAsync();

        public async Task<Role?> GetRoleByIdAsync(int id) => await _unitOfWork.Roles.GetByIdAsync(id);

        public async Task AddRoleAsync(Role role)
        {
            await _unitOfWork.Roles.AddAsync(role);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateRoleAsync(Role role)
        {
            await _unitOfWork.Roles.UpdateAsync(role);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteRoleAsync(int id)
        {
            var role = await _unitOfWork.Roles.GetByIdAsync(id);
            if (role != null)
            {
                await _unitOfWork.Roles.RemoveAsync((role));
                await _unitOfWork.SaveChangesAsync();
            }
        }

        public IEnumerable<Role> GetAllRoleWIthPaging(int start, int length, string search, out int totalRecords)
        {
            var query = _unitOfWork.Roles.GetAllRolewithPaging();
            if (!string.IsNullOrEmpty(search))
            {
                query= query.Where(e=> e.RoleName.Contains(search));
            }

            totalRecords = query.Count();

            return query
                .OrderBy(e => e.Id)
                .Skip(start)
                .Take(length)
                .ToList();
        }

    }
}
