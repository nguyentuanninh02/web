﻿using KS.Common.Response;
using KS.Domain.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace KS.WebApp.Controllers
{
    public class RoleController : Controller
    {
        [Route("danh-sach-quyen")]
        public async Task<ActionResult> Index()
        {
            var tableResponse= await GetAllRoleWithPaging();

            var viewModel = new TableResponse<RoleDto>
            {
                Code = tableResponse.Code,
                Success = tableResponse.Success,
                Message = tableResponse.Message,
                DataError = tableResponse.DataError,
                Draw = tableResponse.Draw,
                RecordsTotal = tableResponse.RecordsTotal,
                RecordsFiltered = tableResponse.RecordsFiltered,
                Data = new List<RoleDto>()
            };

            foreach(var item in tableResponse.Data)
            {
                viewModel.Data.Add(new RoleDto
                {
                    Id = item.Id,
                    RoleName = item.RoleName,
                });
            }

            return View(viewModel);
        }

        public async Task<TableResponse<RoleDto>> GetAllRoleWithPaging()
        {
            TableResponse<RoleDto> tableResponse= new TableResponse<RoleDto>();

            using (var client= new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:7238");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("api/Role/GetAllRoleWithPaging");
                if(response.IsSuccessStatusCode)
                {
                    string jsonResponse= await response.Content.ReadAsStringAsync();
                    tableResponse = JsonConvert.DeserializeObject<TableResponse<RoleDto>>(jsonResponse);
                }
                else{
                    tableResponse.Code= (int)response.StatusCode;
                    tableResponse.Success= false;
                    tableResponse.Message = "failed"; 
                }
            }
            return tableResponse;
        }
    }
}
