﻿using KS.WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace KS.WebApp.Context
{
    public class KSContext : DbContext
    {
        public KSContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> users { get; set; }
    }
}
