﻿using Infastructure.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infastructure.Entity
{
    public class Role: BaseEntity
    {
        public string RoleName { get; set; }
    }
}
