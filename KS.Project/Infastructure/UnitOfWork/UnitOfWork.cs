﻿using Infastructure.Context;
using Infastructure.Entity;
using Infastructure.Repositorys;
using Infastructure.Repositorys.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WalletContext _context;

        public UnitOfWork(WalletContext context)
        {
            _context = context;
            Users= new UserRepository(_context);
            Roles= new RoleRepository(_context);
        }

        public IUserRepository Users {  get; }

        public IRoleRepository Roles {  get; }
        public void Dispose()
        {
            _context.Dispose();   
        }

        public Task<int> SaveChangesAsync()
        {
               return _context.SaveChangesAsync();
        }
    }
}
