﻿using Infastructure.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infastructure.Configuration
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User));  
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x=> x.CreateTime).HasDefaultValue(DateTime.UtcNow);
            builder.Property(x=> x.UpdateTime).HasDefaultValue(DateTime.UtcNow);
            builder.HasOne(x=> x.Role).WithMany().HasForeignKey(x=>x.RoleId);
        }
    }
}
