﻿using Infastructure.Context;
using Infastructure.Entity;
using Infastructure.Repositorys.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infastructure.Repositorys
{
    public class RoleRepository : GenericRepository<Role>, IRoleRepository
    {
        public RoleRepository(WalletContext context) : base(context)
        {
        }

        public IQueryable<Role> GetAllRolewithPaging()
        {
            return _context.Roles;
        }
    }
}
