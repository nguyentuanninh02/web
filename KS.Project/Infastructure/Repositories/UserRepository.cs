﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infastructure.Context;
using Infastructure.Entity;
using Infastructure.Repositorys.IRepository;
using Microsoft.EntityFrameworkCore;

namespace Infastructure.Repositorys
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(WalletContext context) : base(context)
        {
        }

        public Task<User?> GetUserWithNoTrackingAsync(int id)
        {
            return _context.Users.AsNoTracking().FirstOrDefaultAsync(u => u.Id == id);
        }

        public Task<User?> GetUserWithRoleByIdAsync(int id)
        {
            return _context.Users.Include(u=> u.Role).FirstOrDefaultAsync(u => u.Id == id);           
        }
    }
}
