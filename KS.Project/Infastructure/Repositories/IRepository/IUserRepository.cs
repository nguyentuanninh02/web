﻿using Infastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infastructure.Repositorys.IRepository
{
    public interface IUserRepository: IGenericRepository<User>
    {
        Task<User?> GetUserWithRoleByIdAsync(int id);
        Task<User?> GetUserWithNoTrackingAsync(int id);
    }
}
