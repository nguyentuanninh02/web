﻿using Infastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infastructure.Repositorys.IRepository
{
    public interface IRoleRepository: IGenericRepository<Role>
    {
        IQueryable<Role> GetAllRolewithPaging();
    }
}
