﻿using AutoMapper;
using Infastructure.Entity;
using KS.Common.Response;
using KS.Domain.Dtos;
using KS.Domain.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KS.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;

        public RoleController(IRoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllRoles()
        {
            try
            {
                var roles = await _roleService.GetAllRolesAsync();
                var roleDtos = _mapper.Map<IEnumerable<RoleDto>>(roles);
                return Ok(roleDtos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRoleById(int id)
        {
            try
            {
                var role = await _roleService.GetRoleByIdAsync(id);
                if (role == null)
                    return NotFound();

                var roleDto = _mapper.Map<RoleDto>(role);
                return Ok(roleDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(RoleDto roleDto)
        {
            try
            {
                if (roleDto == null)
                {
                    return BadRequest();
                }
                var roleExsit = await _roleService.GetRoleByIdAsync(roleDto.Id);
                if (roleExsit != null)
                {
                    return BadRequest("Role realy exsit");
                }
                var role = _mapper.Map<Role>(roleDto);
                await _roleService.AddRoleAsync(role);
                return CreatedAtAction(nameof(GetRoleById), new { id = role.Id }, roleDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateRole(int id, RoleDto roleDto)
        {
            try
            {
                if (roleDto == null || id != roleDto.Id)
                    return BadRequest();
   
                var role = _mapper.Map<Role>(roleDto);
                await _roleService.UpdateRoleAsync(role);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRole(int id)
        {
            try
            {
                if (_roleService.GetRoleByIdAsync(id).Result == null)
                {
                    return NotFound();
                }
                await _roleService.DeleteRoleAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetAllRoleWithPaging")]
        public TableResponse<RoleDto> GetAllRoleWithPaging(int start = 0, int length= 10, string search = "")
        {
            int totalRecords;
            var getAllRoleWithPaging = _roleService.GetAllRoleWIthPaging(start, length, search, out totalRecords);
            var data= _mapper.Map<List<RoleDto>>(getAllRoleWithPaging);

            var response = new TableResponse<RoleDto>
            {
                Draw = 0,
                RecordsTotal = totalRecords,
                RecordsFiltered = totalRecords,
                Data = data,
            };
            return response;
        }
    }
}
