﻿using AutoMapper;
using StudentManagement.Domain.Dto;
using StudentManagement.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Domain.Mapper
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile() { 
            CreateMap<Course, CourseDto>().ReverseMap();
            CreateMap<Student, StudentDto>().ReverseMap();
            CreateMap<Student, StudentCreateDto>().ReverseMap();
            CreateMap<Enrollment, EnrollmentDto>()
                .ForMember(dest => dest.CourseName, opt => opt.MapFrom(src => src.Course.Title))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Student.UserName));
            CreateMap<Enrollment, EnrollmentCreateDto>().ReverseMap();
        }
    }
}
