﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Domain.Dto
{
    public class EnrollmentDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string CourseName { get; set; }
        public decimal? Grade { get; set; }
    }
}
