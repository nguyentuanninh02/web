﻿using StudentManagement.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Domain.Service.IService
{
    public interface IEnrollmentService
    {
        Task<IEnumerable<Enrollment>> GetAllEnrollmentsAsync();
        Task<Enrollment?> GetEnrollmentByIdAsync(int id);
        Task AddEnrollmentAsync(Enrollment role);
        Task UpdateEnrollmentAsync(Enrollment role);
        Task DeleteEnrollmentAsync(int id);
    }
}
