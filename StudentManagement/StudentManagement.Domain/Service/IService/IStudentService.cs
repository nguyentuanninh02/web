﻿using StudentManagement.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Domain.Service.IService
{
    public interface IStudentService
    {
        Task<IEnumerable<Student>> GetAllStudentsAsync();
        Task<Student?> GetStudentByIdAsync(int id);
        Task AddStudentAsync(Student role);
        Task UpdateStudentAsync(Student role);
        Task DeleteStudentAsync(int id);
        Task<string> GenerateUserName(string firstName, string lastName);
        Task<int> GetIdByUserName(string userName);
    }
}
