﻿using StudentManagement.Domain.Service.IService;
using StudentManagement.Infrastructure.Entity;
using StudentManagement.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Domain.Service
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StudentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddStudentAsync(Student student)
        {
            student.UserName = await GenerateUserName(student.FirstName, student.LastName);
            await _unitOfWork.Student.AddAsync(student);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteStudentAsync(int id)
        {
            var student= await _unitOfWork.Student.GetByIdAsync(id);
            if(student!= null)
            {
                await _unitOfWork.Student.RemoveAsync(student);
                await _unitOfWork.SaveChange();
            }
            
        }

        public async Task<IEnumerable<Student>> GetAllStudentsAsync()
        {
            return await _unitOfWork.Student.GetAllAsync();
        }

        public async Task<Student?> GetStudentByIdAsync(int id)
        {
            return await _unitOfWork.Student.GetByIdAsync(id);
        }

        public async Task UpdateStudentAsync(Student student)
        {
            student.UserName = await GenerateUserName(student.FirstName, student.LastName);
            await _unitOfWork.Student.UpdateAsync(student);
            await _unitOfWork.SaveChange();
        }
        
        public async Task<string> GenerateUserName(string firstName, string lastName)
        {
            var lastNameInitials = string.Concat(lastName.Split(' ').Select(s => s[0]));
            var username = $"{firstName.ToLower()}{lastNameInitials.ToLower()}";
            int suffix = await _unitOfWork.Student.CountByUserNameAsync(username);
            if (suffix > 0)
            {
                username += suffix+ 1;
            }

            return username;
        }

        public async Task<int> GetIdByUserName(string userName)
        {
            var student = await _unitOfWork.Student.GetAsync(x => x.UserName== userName);
            if (student == null)
            {
                return -1;
            }
            return student.Id;
               
        }
    }
}
