﻿using StudentManagement.Domain.Service.IService;
using StudentManagement.Infrastructure.Entity;
using StudentManagement.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Domain.Service
{
    public class CourseService : ICourseService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CourseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddCourseAsync(Course role)
        {
            await _unitOfWork.Course.AddAsync(role);
            await _unitOfWork.SaveChange();
        }

        public async Task DeleteCourseAsync(int id)
        {
            var course = await _unitOfWork.Course.GetByIdAsync(id);
            if (course == null)
            {
                return;
            }
            await _unitOfWork.Course.RemoveAsync(course);
            await _unitOfWork.SaveChange();
        }

        public async Task<IEnumerable<Course>> GetAllCoursesAsync()
        {
            return await _unitOfWork.Course.GetAllAsync();
        }

        public async Task<Course?> GetCourseByIdAsync(int id)
        {
            return await _unitOfWork.Course.GetByIdAsync(id);
        }

        public async Task UpdateCourseAsync(Course role)
        {
            await _unitOfWork.Course.UpdateAsync(role);
            await _unitOfWork.SaveChange();
        }
    }
}
