﻿using StudentManagement.Domain.Service.IService;
using StudentManagement.Infrastructure.Entity;
using StudentManagement.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Domain.Service
{
    public class EnrollmentService : IEnrollmentService
    {
        private readonly IUnitOfWork _unitOfWork;
        public EnrollmentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddEnrollmentAsync(Enrollment enrollment)
        {
            await _unitOfWork.Enrollment.AddAsync(enrollment);

        }

        public async Task DeleteEnrollmentAsync(int id)
        {
            var role = await _unitOfWork.Enrollment.GetByIdAsync(id);
            if (role != null)
            {
                await _unitOfWork.Enrollment.RemoveAsync(role);
                await _unitOfWork.SaveChange();
            }
        }

        public async Task<IEnumerable<Enrollment>> GetAllEnrollmentsAsync()
        {
            return await _unitOfWork.Enrollment.GetAllAsync(includeProperties: "Student,Course");
        }

        public async Task<Enrollment?> GetEnrollmentByIdAsync(int id)
        {
            return await _unitOfWork.Enrollment.GetAsync(x=> x.Id== id, includeProperties: "Student,Course");
        }

        public async Task UpdateEnrollmentAsync(Enrollment enrollment)
        {
            await _unitOfWork.Enrollment.UpdateAsync(enrollment);
            await _unitOfWork.SaveChange();
        }
    }
}
