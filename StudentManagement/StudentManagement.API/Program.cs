using Microsoft.EntityFrameworkCore;
using StudentManagement.Domain.Service;
using StudentManagement.Domain.Service.IService;
using StudentManagement.Infrastructure.Context;
using StudentManagement.Infrastructure.Repository;
using StudentManagement.Infrastructure.Repository.IRepository;
using StudentManagement.Infrastructure.UnitOfWork;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<ApplicationContext>(option => option.UseMySql(builder.Configuration.GetConnectionString("WalletConnection"), ServerVersion.Parse("8.4.0-mysql")));

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
builder.Services.AddScoped<IStudentService, StudentService>();
builder.Services.AddScoped<ICourseService, CourseService>();
builder.Services.AddScoped<IEnrollmentService, EnrollmentService>();

builder.Services.AddAutoMapper(typeof(StudentManagement.Domain.Mapper.AutoMapperProfile));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
