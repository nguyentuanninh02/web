﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentManagement.Domain.Dto;
using StudentManagement.Domain.Service.IService;
using StudentManagement.Infrastructure.Entity;

namespace StudentManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnrollmentController : ControllerBase
    {
        private readonly IEnrollmentService _enrollmentService;
        private readonly IStudentService _studentService;
        private readonly ICourseService _courseService;
        private readonly IMapper mapper;

        public EnrollmentController(IEnrollmentService enrollmentService,IStudentService studentService,ICourseService courseService ,IMapper mapper)
        {
            _enrollmentService = enrollmentService;
            _studentService = studentService;
            _courseService = courseService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllEnrollments()
        {
            try
            {
                var enrollments = await _enrollmentService.GetAllEnrollmentsAsync();
                var enrollmentsDto = mapper.Map<IEnumerable<EnrollmentDto>>(enrollments);
                return Ok(enrollmentsDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEnrollmentById(int id)
        {
            try
            {
                var enrollment = await _enrollmentService.GetEnrollmentByIdAsync(id);
                if (enrollment == null)
                {
                    return NotFound();
                }

                var enrollmentDto = mapper.Map<EnrollmentDto>(enrollment);
                return Ok(enrollmentDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddEnrollment(EnrollmentCreateDto enrollmentDto)
        {
            try
            {
                if (enrollmentDto == null)
                {
                    return BadRequest();
                }
                if(await _enrollmentService.GetEnrollmentByIdAsync(enrollmentDto.Id) != null)
                {
                    return BadRequest("Enrollment already exists");
                }
                if(await _courseService.GetCourseByIdAsync(enrollmentDto.CourseId) == null)
                {
                    return BadRequest("Course not found");
                }

                var enrollment = mapper.Map<Enrollment>(enrollmentDto);
                int studentId = await _studentService.GetIdByUserName(enrollmentDto.UserName);

                if(studentId == -1)
                {
                    return BadRequest("Student not found");
                }
                enrollment.StudentId = studentId;
                await _enrollmentService.AddEnrollmentAsync(enrollment);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEnrollment(int id, EnrollmentCreateDto enrollmentDto)
        {
            try
            {
                if (enrollmentDto == null || id != enrollmentDto.Id)
                {
                    return BadRequest("Enrollment is null");
                }
                var enrollment = mapper.Map<Enrollment>(enrollmentDto);
                int studentId = await _studentService.GetIdByUserName(enrollmentDto.UserName);

                if(studentId == -1)
                {
                    return BadRequest("Student not found");
                }
                enrollment.StudentId = studentId;

                await _enrollmentService.UpdateEnrollmentAsync(enrollment);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEnrollment(int id)
        {
            try
            {
                if(await _enrollmentService.GetEnrollmentByIdAsync(id) == null)
                {
                    return NotFound();
                }
                await _enrollmentService.DeleteEnrollmentAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
