﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentManagement.Domain.Dto;
using StudentManagement.Domain.Service.IService;
using StudentManagement.Infrastructure.Entity;

namespace StudentManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourseService _courseService;
        private readonly IMapper _mapper;

        public CourseController(ICourseService courseService, IMapper mapper)
        {
            _courseService = courseService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCourses()
        {
            try
            {
                var courses = await _courseService.GetAllCoursesAsync();
                var coursesDto = _mapper.Map<IEnumerable<CourseDto>>(courses);
                return Ok(coursesDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCourseById(int id)
        {
            try
            {
                var course = await _courseService.GetCourseByIdAsync(id);
                if (course == null)
                {
                    return NotFound();
                }

                var courseDto = _mapper.Map<CourseDto>(course);
                return Ok(courseDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddCourse(CourseDto courseDto)
        {
            try
            {
                if(courseDto == null)
                {
                    return BadRequest("Course is null");
                }
                if(await _courseService.GetCourseByIdAsync(courseDto.Id) != null)
                {
                    return BadRequest("Course already exists");
                }
                var course = _mapper.Map<Course>(courseDto);
                await _courseService.AddCourseAsync(course);
                return CreatedAtAction(nameof(GetCourseById), new { id = course.Id }, course);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCourse(int id, CourseDto courseDto)
        {
            try
            {
                if (courseDto == null || id != courseDto.Id)
                {
                    return BadRequest("Course is null");
                }
                var course = _mapper.Map<Course>(courseDto);
                await _courseService.UpdateCourseAsync(course);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCourse(int id)
        {
            try
            {
                if(await _courseService.GetCourseByIdAsync(id) == null)
                {
                    return NotFound();
                }
                await _courseService.DeleteCourseAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
