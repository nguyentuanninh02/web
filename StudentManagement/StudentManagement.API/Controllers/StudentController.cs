﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentManagement.Domain.Dto;
using StudentManagement.Domain.Service.IService;
using StudentManagement.Infrastructure.Entity;

namespace StudentManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _studentService;
        private readonly IMapper mapper;

        public StudentController(IStudentService studentService, IMapper mapper)
        {
            _studentService = studentService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStudents()
        {
            try
            {
                var students = await _studentService.GetAllStudentsAsync();
                var studentsDto = mapper.Map<IEnumerable<StudentDto>>(students);
                return Ok(studentsDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetStudentById(int id)
        {
            try
            {
                var student = await _studentService.GetStudentByIdAsync(id);
                if (student == null)
                {
                    return NotFound();
                }

                var studentDto = mapper.Map<StudentDto>(student);
                return Ok(studentDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddStudent(StudentCreateDto studentCreateDto)
        {
            try
            {
                if(studentCreateDto == null)
                {
                    return BadRequest("Student object is null");
                }
                if(await _studentService.GetStudentByIdAsync(studentCreateDto.Id) != null)
                {
                    return BadRequest("Student already exists");
                }
                var student = mapper.Map<Student>(studentCreateDto);
                await _studentService.AddStudentAsync(student);
                return CreatedAtAction(nameof(GetStudentById), new { id = student.Id }, student); 
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateStudent(int id, StudentCreateDto studentDto)
        {
            try
            {
                if (studentDto == null || id != studentDto.Id)
                {
                    return BadRequest("Student object is null");
                }
                var student = mapper.Map<Student>(studentDto);
                await _studentService.UpdateStudentAsync(student);
                return Ok(student);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            try
            {
                var student = await _studentService.GetStudentByIdAsync(id);
                if (student == null)
                {
                    return NotFound();
                }
                await _studentService.DeleteStudentAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
    }
}
