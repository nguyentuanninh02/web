﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Infrastructure.Configuration
{
    public class EnrollmentConfig : IEntityTypeConfiguration<Enrollment>
    {
        public void Configure(EntityTypeBuilder<Enrollment> builder)
        {
            builder.ToTable(nameof(Enrollment));
            builder.HasKey(x => x.Id);
            builder.HasOne(x=> x.Student).WithMany().HasForeignKey(x => x.StudentId);
            builder.HasOne(x => x.Course).WithMany().HasForeignKey(x => x.CourseId);
            builder.Property(x=> x.EnrollmentDate).HasDefaultValue(DateTime.Now);
        }
    }
}
