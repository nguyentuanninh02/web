﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Infrastructure.Configuration
{
    public class StudentConfig : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable(nameof(Student));
            builder.HasKey(t => t.Id);
            builder.Property(t => t.FirstName).HasMaxLength(50);
            builder.Property(t=> t.LastName).HasMaxLength(50);
            builder.Property(t => t.UserName).HasMaxLength(50);
        }
    }
}
