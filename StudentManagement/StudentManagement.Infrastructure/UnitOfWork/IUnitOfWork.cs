﻿using StudentManagement.Infrastructure.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Infrastructure.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICourseRepository Course{ get; }
        IEnrollmentRepository Enrollment{ get; }
        IStudentRepository Student{ get; }
        Task<int> SaveChange();
    }
}
