﻿using StudentManagement.Infrastructure.Context;
using StudentManagement.Infrastructure.Repository;
using StudentManagement.Infrastructure.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
            Course= new CourseRepository(_context);
            Enrollment= new EnrollmentRepository(_context);
            Student= new StudentRepository(_context);
        }

        public ICourseRepository Course{ get; set; }
        public IEnrollmentRepository Enrollment{ get; set; }
        public IStudentRepository Student { get; set; }

        public void Dispose()
        {
            _context.Dispose();    
        }

        public Task<int> SaveChange()
        {
            return _context.SaveChangesAsync();
        }
    }
}
