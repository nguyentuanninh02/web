﻿using StudentManagement.Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Infrastructure.Repository.IRepository
{
    public interface IEnrollmentRepository: IGenericRepository<Enrollment>
    {
    }
}
