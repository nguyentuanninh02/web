﻿using StudentManagement.Infrastructure.Context;
using StudentManagement.Infrastructure.Entity;
using StudentManagement.Infrastructure.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Infrastructure.Repository
{
    public class EnrollmentRepository : GenericRepository<Enrollment>, IEnrollmentRepository
    {
        public EnrollmentRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
