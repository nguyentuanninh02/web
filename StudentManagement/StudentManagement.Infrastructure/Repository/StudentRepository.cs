﻿using Microsoft.EntityFrameworkCore;
using StudentManagement.Infrastructure.Context;
using StudentManagement.Infrastructure.Entity;
using StudentManagement.Infrastructure.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Infrastructure.Repository
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        public StudentRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<int> CountByUserNameAsync(string userName)
        {
            return await _context.students.CountAsync(s => s.UserName == userName);
        }
    }
}
